<h1>Listado de Talleres</h1>
<?php if ($talleres) : //print_r($instructores);
?>
  <div class=" col-sm-11 table-responsive">
    <table class="table table-bordered table-hover" id="tbl_Talleres">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Duracion</th>
          <th>Costo</th>
          <th>Contenido</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <!-- 
                Setentecias selectivas
                Sentencias repetitivas
                -for
                -while
                -do while
                -foreach
            -->
      <tbody>
        <?php foreach ($talleres as $filaTemporal) : ?>
          <tr>
            <td>
              <?php echo $filaTemporal->id_semi; ?>
            </td>
            <td>
              <?php echo $filaTemporal->nom_semi; ?>
            </td>
            <td>
              <?php echo $filaTemporal->dur_semi; ?>
            </td>
            <td>
              <?php echo $filaTemporal->cont_semi; ?>
            </td>
            <td class="text-center">
              <?php if ($filaTemporal->cont_semi != "") : ?>
                <!-- Si no esta vacio de muestra la foto -->
                <a href="<?php echo base_url('uploads/contenido/') . $filaTemporal->cont_semi; ?>" download="<?php echo $filaTemporal->cont_semi ?>">
                  <?php echo $filaTemporal->cont_semi ?>
                </a>
              <?php else : ?>
                <!-- Esta vacio N/A -->
                <img src="<?php echo base_url('uploads/404-error.jpg') ?>" alt="Eror seminario no insertada">
              <?php endif ?>
            </td>
            <td class="text-center">
              <a type="button" data-id="<?php echo $filaTemporal->id_semi;  ?>" title="Editar Seminario" data-toggle="modal" data-target="#modalEditarSeminario" onclick="cargarEditor()"><i class="mdi mdi-grease-pencil"></i></a>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR") : ?>
                <?php $id = $filaTemporal->id_semi; ?>
                <a href="<?php echo site_url('/talleres/eliminar/') . $filaTemporal->id_semi; ?>" title="Eliminar Seminario"><i class="mdi mdi-eraser" style="color: red;"></i></a>
              <?php endif; ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>

<?php else : ?>
  <h1>No hay datos</h1>
<?php endif; ?>

<!-- SCRIPTS DEL DATA TABLE -->
<script>
  //$("#tbl_Talleres").DataTable();
</script>

<script>
  function cargarEditor() {
    $('#modalEditarSeminario').on('show.bs.modal', function(event) {
      var button = $(event.relatedTarget);
      var id = button.data('id');
      var ruta = "<?php echo site_url('talleres/editar/'); ?>"+ id;
      $("#cargar-Editor").load(ruta);
      // Asigna el ID a algún campo en la ventana modal
      $('#modal-id').text(id);
      // Realiza otras acciones en la ventana modal según sea necesario
    });


  }
  cargarEditor();
</script>

<!-- Modal -->
<div class="modal fade" id="modalEditarSeminario" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editar Laboratorio</h4>&nbsp;
        <h4 class="modal-title"><span id="modal-id"></span></h4>
        <button type="button" class="close text-light" data-dismiss="modal">&times;</button>
      </div>
      <div id="cargar-Editor">
        
      </div>

    </div>
  </div>
</div>

