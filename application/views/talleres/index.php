<div class="row">
  <div class="col-sm-6">
    <h1 class="text-center">Listado de Seminarios</h1>
  </div>
  <div class="col-sm-6 justify-content-center">
  
  </div>
  <button type="button" class="btn btn-success" onclick="cargarSeminarios()" name="button">Recargar</button>
  
</div>
<div class="row">
  <div class="col-sm-12" id="contenedor-seminarios">

  </div>
</div>
<hr>

<div class="#">
  <h2>Ingresar nuevo seminario</h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalNuevoSeminario">Agregar Seminario</button>

  <!-- Modal -->
  <div class="modal fade" id="modalNuevoSeminario" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Nuevo Seminario</h4>
          <button type="button" class="close text-light" data-dismiss="modal">&times;</button>
        </div>
        <form class="" action="#" method="post" id="frm_nuevo_seminario" enctype="multipart/form-data">
          <div class="modal-body">

            <hr>
            <div class="row">

              <div class="col-sm-4">
                <label for="">Nombre del seminario:<span class="obligatorio">(Obligatorio)</span></label>
                <input class="form-control text-center" type="text" name="nom_semi" id="nom_semi" placeholder="Ingrese el nombre del seminario" required>
              </div>

              <div class="col-sm-4">
                <label for="">Duracion del seminario:<span class="obligatorio">(Obligatorio)</span></label>
                <input class="form-control text-center" type="text" name="dur_semi" id="dur_semi" placeholder="Ingrese la duracion del seminario" required>
              </div>

              <div class="col-sm-4">
                <label for="">Costo del seminario:<span class="obligatorio">(Obligatorio)</span></label>
                <input class="form-control text-center" type="number" name="cos_semi" id="cos_semi" placeholder="Ingrese la duracion del seminario" required>
              </div>

            </div>
            <hr>
            <div class="row justify-content-center align-items-center ">
              <div class="col-sm-6 text-center">
                <label for="">Contenido<span class="obligatorio">(Obligatorio)</span></label>
                <input class="form-control text-center" type="file" name="cont_semi" id="cont_semi" required>
              </div>
            </div>
            <br>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="submit" name="button" class="btn btn-primary">Guardar</button>
              &nbsp;
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- SRIPT DEL INGRESO DE DATOS -->
<script>
  $("#contenido_wslc").fileinput({
    language: "es",
    //allowedFileExtenions:[".jpg",".png",".gif"],
  });
</script> 
<script type="text/javascript">
  $("#frm_nuevo_seminario").validate({
    rules: {
        nom_semi: {
        required: true,
        minlength: 10,
        maxlength: 50,
        letras:true
      },
      dur_semi: {
        required: true,
        minlength: 3,
        maxlength: 250,
      },
      cos_semi: {
        required: true,
        minlength: 1,
        maxlength: 3,
        max: 500,
        min: 1
      },
      cont_semi: {
        required: true,
        minlength: 3,
        maxlength: 250,
      }
    },
    messages: {
      nom_semi: {
        required: "Ingrese el nombre del seminario",
        minlength: "Seminario incorrecto, ingrese minimo 10 digitos",
        maxlength: "Seminario incorrecto, ingrese maximo 10 digitos",
      },
      dur_semi: {
        required: "Ingrese la duracion del seminario",
        minlength: "La duracion debe tener 3 letras",
        maxlength: "Duracion incorrecta",
      },
      cos_semi: {
        required: "Ingrese el costo del seminario",
        minlength: "El costo debe tener almenos 1 digito",
        maxlength: "Costo del seminario incorrecto",
        max: "Ingrese un valor aceptable",
        min: "Ingrese un valor mayor 0 igual a 1"
      },
      cont_semi: {
        required: "Inserte el contenido en formato pdf",
        minlength: "3",
        maxlength: "250",
      }
    },
    submitHandler: function(formulario) {
      var datosIngresados_file = new FormData(formulario);
      var datosIngresados = $(formulario).serialize();
      //perro 
      $.each(datosIngresados.split('&'), function(index, item) {
        var keyValue = item.split('=');
        datosIngresados_file.append(keyValue[0], decodeURIComponent(keyValue[1]));
      });

      //alert(datosIngresados);
      $.ajax({
        url: "<?php echo site_url('talleres/guardar') ?>",
        type: 'POST',
        data: datosIngresados_file,
        processData: false, // Desactivar el procesamiento de datos
        contentType: false, 
        success: function(data) {
          //alert(data);
          toastr.success("Seminario insertado exitosamente");
          $("#modalNuevoSeminario").modal("hide");
          $("#nom_semi").val("");
          $("#dur_semi").val("");
          $("#cos_semi").val("");
          $("#cont_semi").val("");
          cargarSeminarios();
        }
      })
    }
  });
</script> 
<!-- Script para Cargar listado de laboratorios -->
<script>
  function cargarSeminarios() {
    $("#contenedor-seminarios").load(
      "<?php echo site_url('talleres/listado'); ?>"
    );
  }
  cargarSeminarios();
</script>
