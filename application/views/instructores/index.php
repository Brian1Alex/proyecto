<div class="row">
    <div class="col-md-8">
        <h1 Align="center">LISTADO DE INSTRUCTORES</h1>
    </div>
    <div class="col-md-4">
        <a href="<?php echo site_url('instructores/nuevo') ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"> Agregar Instructor</i></a>
    </div>
</div>
<br>
<?php if ($instructores) : ?>
    <table class="table table-striped table-border table-hover" id="tbl_instructores">
        <thead>
            <tr>
                <th>ID</th>
                <th>Foto</th>
                <th>Cedula</th>
                <th>Primer Apellido</th>
                <th>Segundo Apellido</th>
                <th>Nombres</th>
                <th>Titulo</th>
                <th>Telefono</th>
                <th>Dirección</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($instructores as $filaTemporal) : ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_ins; ?>
                    </td>
                    <td>
                        <?php if ($filaTemporal->foto_ins != "") : ?>
                            <a href="<?php echo base_url('uploads/') . $filaTemporal->foto_ins; ?>" target="_blank">
                                <img src="<?php echo base_url('uploads/') . $filaTemporal->foto_ins; ?>" alt="">
                            </a>
                        <?php else : ?>
                            <img src="<?php echo base_url('assets/images/perfil.png') ?>" alt="">
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->cedula_ins; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->primer_apellido_ins; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->segundo_apellido_ins; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombres_ins; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->titulo_ins; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->telefono_ins; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->direccion_ins; ?>
                    </td>

                    <td class="text-center">
                        <a href="<?php echo site_url(); ?>/Instructores/editar/<?php echo $filaTemporal->id_ins; ?>" title="Editar Instructor"><i class="mdi mdi-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <?php if ($this->session->userdata("conectado")->perfil_usu == "Administrador") : ?>
                            <a href="<?php echo site_url(); ?>/Instructores/eliminar/<?php echo $filaTemporal->id_ins; ?>" title="Eliminar Instructor" onclick="return confirm('¿Seguro dese eliminar este registro?');" style="color:red;">
                                <i class="mdi mdi-delete"></i>
                                Eliminar
                            </a>

                        <?php endif; ?>

                    </td>
                </tr>

            <?php endforeach; ?>

        </tbody>
    </table>
<?php else : ?>
    <h1>No posee Instructores</h1>
<?php endif; ?>


<script type="text/javascript">
    $("#tbl_instructores").DataTable();
</script>