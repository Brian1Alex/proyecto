<h1>NUEVO INSTRUCTOR</h1>
<form class="" id="frm_nuevo_instructor" action="<?php echo site_url(); ?>/instructores/guardar" method="post" enctype="multipart/form-data">
  <div class="row">
    <div class="col-md-4">
      <label for="">Cédula:
        <span class="obligatorio">(Obligatorio)</span>
      </label>
      <br>
      <input type="number" placeholder="Ingrese la cédula" class="form-control" required min="99999999" name="cedula_ins" value="">
    </div>
    <div class="col-md-4">
      <label for="">Primer Apellido: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese el primer apellido" class="form-control" required name="primer_apellido_ins" value="">
    </div>
    <div class="col-md-4">
      <label for="">Segundo Apellido:</label>
      <br>
      <input type="text" placeholder="Ingrese el segundo apellido" class="form-control" name="segundo_apellido_ins" value="">
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4">
      <label for="">Nombre: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese los nombres" class="form-control" required name="nombres_ins" value="">
    </div>
    <div class="col-md-4">
      <label for="">Título: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese el titulo" class="form-control" required name="titulo_ins" value="">
    </div>
    <div class="col-md-4">
      <label for="">Teléfono: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="number" placeholder="Ingrese el telefono" class="form-control" required min="99999999" name="telefono_ins" value="">
    </div>
  </div>

  <br>
  <div class="row">
    <div class="col-md-12">
      <label for="">Dirección: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese la direccion" class="form-control" required name="direccion_ins" value="">
    </div>
  </div>
  <br>

  <div class="row" Align="center">
    <div class="col-md-2"></div>

    <div class="col-md-8">
      <label for="">Foto: </label>
      <input type="file" name="foto_ins" id="foto_ins">
    </div>
    <div class="col-md-2"></div>
  </div>


  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">
        Guardar
      </button>
      &nbsp;
      <a href="<?php echo site_url(); ?>/instructores/index" class="btn btn-danger">Cancelar</a>
    </div>
  </div>
</form>

<script type="text/javascript">
  $("#frm_nuevo_instructor").validate({
    rules: {
      cedula_ins: {
        required: true,
        minlength: 10,
        maxlength: 10,
        digits: true,
      },
      primer_apellido_ins: {
        required: true,
        minlength: 3,
        maxlength: 250,
        letras: true,
      },
      segundo_apellido: {
        letras: true,
      },
      nombres_ins: {
        required: true,
        minlength: 5,
        maxlength: 250,
        letras: true,
      },
      titulo_ins: {
        required: true,
        minlength: 3,
        maxlength: 150,
        letras: true,
      },
      telefono_ins: {
        required: true,
        minlength: 10,
        maxlength: 10,
      },

    },
    messages: {
      cedula_ins: {
        required: "Por favor ingrese el número de cedula",
        minlength: "Cédula incorrecta, ingrese 10 digitos",
        maxlength: "Cédula incorrecta, ingrese 10 digitos",
        digits: "Este campo solo acepta números",
        number: "Este campo solo acepta números",
      },
      primer_apellido_ins: {
        required: "Por favor ingrese el primer apellido",
        minlength: "El apellido debe tener al menos 3 caracteres",
        maxlength: "Apellido incorrecto",
      },
      segundo_apellido: {

      },
      nombres_ins: {
        required: "Por favor ingresar un nombre",
        minlength: "El apellido debe tener al menos 3 caracteres",
        maxlength: "Nombre incorrecto",
      },
      titulo_ins: {
        required: "Por favor ingresar un titulo obtenido",
        minlength: "El titulo debe tener al menos 3 caracteres",
        maxlength: "Titulo muy largo",
      },
      telefono_ins: {
        required: "Por favor ingresa un número de telefono",
        minlength: "Cédula incorrecta, ingrese 10 digitos",
        maxlength: "Cédula incorrecta, ingrese 10 digitos",
        digits: "Este campo solo acepta números",
        number: "Este campo solo acepta números",
      },

    }
  });
</script>

<script type="text/javascript">
  $("#foto_ins").fileinput({
    language: 'es'
  });
</script>