<hr>
<h1 Align="center">INGRESAR UN AUTOMOVIL</h1>
<hr>
<form class="" action="<?php echo site_url(); ?>/instructores/guardaraut" method="post">
    <div class="row">
        <div class="col-md-4">
            <label for="">Marca</label>
            <br>
            <input type="text" placeholder="Ingresar la Marca" class="form-control" name="nom_aut" value="">
        </div>
        <div class="col-md-4">
            <label for="">Color</label>
            <br>
            <input type="text" placeholder="Ingrese el Color" class="form-control" name="col_aut" value="">
        </div>
        <div class="col-md-4">
            <label for="">Precio</label>
            <br>
            <input type="number" placeholder="Precio comercial" class="form-control" name="pre_aut" value="">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4">
            <label for="">Kilometraje</label>
            <br>
            <input type="number" placeholder="colocar con punto" class="form-control" name="kilo_aut" value="">
        </div>
        <div class="col-md-4">
            <label for="">Año Fabricacion</label>
            <br>
            <input type="number" placeholder="Ingresar solo el año" class="form-control" name="an_aut" value="">
        </div>
        <div class="col-md-4">
            <label for="">Transmisión</label>
            <br>
            <input type="text" placeholder="indique si es Manual o Automatico" class="form-control" name="trans_aut" value="">

        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <label for="">Descripcion</label>
                <br>
                <input type="text" placeholder="ingrese algunas caracteristicas extras del vehiculo" class="form-control" name="desc_aut" value="">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">Guardar</button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/instructores/listadoauto" class="btn btn-danger">Cancelar</a>
        </div>
    </div>

</form>