<h1>EDITAR INSTRUCTOR</h1>
<form class="" id="frm_editar_instructor" action="<?php echo site_url('instructores/proceActuali'); ?>" method="post">
  <div class="row">
    <input type="text" class="form-control" name="id_ins" id="id_ins" hidden value="<?php echo $instructorEditar->id_ins; ?>">

    <div class="col-md-4">
      <label for="">Cédula:
        <span class="obligatorio">(Obligatorio)</span>
      </label>
      <br>
      <input type="number" placeholder="Ingrese la cédula" class="form-control" required min="99999999" name="cedula_ins" value="<?php echo $instructorEditar->cedula_ins; ?>">
    </div>
    <div class="col-md-4">
      <label for="">Primer Apellido: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese el primer apellido" class="form-control" required name="primer_apellido_ins" value="<?php echo $instructorEditar->primer_apellido_ins; ?>">
    </div>
    <div class="col-md-4">
      <label for="">Segundo Apellido:</label>
      <br>
      <input type="text" placeholder="Ingrese el segundo apellido" class="form-control" name="segundo_apellido_ins" value="<?php echo $instructorEditar->segundo_apellido_ins; ?>">
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4">
      <label for="">Nombre: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese los nombres" class="form-control" required name="nombres_ins" value="<?php echo $instructorEditar->nombres_ins; ?>">
    </div>
    <div class="col-md-4">
      <label for="">Título: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese el titulo" class="form-control" required name="titulo_ins" value="<?php echo $instructorEditar->titulo_ins; ?>">
    </div>
    <div class="col-md-4">
      <label for="">Teléfono: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="number" placeholder="Ingrese el telefono" class="form-control" required min="99999999" name="telefono_ins" value="<?php echo $instructorEditar->telefono_ins; ?>">
    </div>
  </div>

  <br>
  <div class="row">
    <div class="col-md-12">
      <label for="">Dirección: <span class="obligatorio">(Obligatorio)</span></label>
      <br>
      <input type="text" placeholder="Ingrese la direccion" class="form-control" required name="direccion_ins" value="<?php echo $instructorEditar->direccion_ins; ?>">
    </div>
  </div>

  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">
        Editar
      </button>
      &nbsp;
      <a href="<?php echo site_url(); ?>/instructores/index" class="btn btn-danger">Cancelar</a>
    </div>
  </div>
</form>

<script type="text/javascript">
  $("#frm_editar_instructor").validate({
    rules: {
      cedula_ins: {
        required: true,
        minlength: 10,
        maxlength: 10,
        digits: true,
      },
      primer_apellido_ins: {
        required: true,
        minlength: 3,
        maxlength: 250,
        letras: true,
      },
      segundo_apellido: {
        letras: true,
      },
      nombres_ins: {
        required: true,
        minlength: 5,
        maxlength: 250,
        letras: true,
      },
      titulo_ins: {
        required: true,
        minlength: 3,
        maxlength: 150,

      },
      telefono_ins: {
        required: true,
        minlength: 10,
        maxlength: 10,
      },
      direccion_ins: {
        required: true,
        minlength: 5,
        maxlength: 300,

      }
    },
    messages: {
      cedula_ins: {
        required: "Por favor ingrese el número de cedula",
        minlength: "Cédula incorrecta, ingrese 10 digitos",
        maxlength: "Cédula incorrecta, ingrese 10 digitos",
        digits: "Este campo solo acepta números",
        number: "Este campo solo acepta números",
      },
      primer_apellido_ins: {
        required: "Por favor ingrese el primer apellido",
        minlength: "El apellido debe tener al menos 3 caracteres",
        maxlength: "Apellido incorrecto",
      },
      segundo_apellido: {

      },
      nombres_ins: {
        required: "Por favor ingresar un nombre",
        minlength: "El apellido debe tener al menos 3 caracteres",
        maxlength: "Nombre incorrecto",
      },
      titulo_ins: {
        required: "Por favor ingresar un titulo obtenido",
        minlength: "El titulo debe tener al menos 3 caracteres",
        maxlength: "Titulo muy largo",
      },
      telefono_ins: {
        required: "Por favor ingresa un número de telefono",
        minlength: "Cédula incorrecta, ingrese 10 digitos",
        maxlength: "Cédula incorrecta, ingrese 10 digitos",
        digits: "Este campo solo acepta números",
        number: "Este campo solo acepta números",
      },
      direccion_ins: {
        required: "Por favor ingresa una Direccion de Domicilio",
        minlength: "La dirección debe tener al menos 5 caracteres",
        maxlength: "Direccion muy Extensa",

      }
    }
  });
</script>