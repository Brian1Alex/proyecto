<h1 Align="center">Listado de Autos</h1>

<?php if ($instructores) : ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE AUTO</th>
                <th>COLOR DEL AUTO</th>
                <th>PRECIO DEL AUTO</th>
                <th>KILOMETRAJE</th>
                <th>AÑO DE FABRICACION</th>
                <th>TIPO DE TRANSMISION</th>
                <th>DESCRIPCION</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($instructores as $filaTemp) : ?>
                <tr>
                    <td>
                        <?php echo $filaTemp->id_aut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemp->nom_aut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemp->col_aut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemp->pre_aut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemp->kilo_aut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemp->an_aut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemp->trans_aut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemp->desc_aut; ?>
                    </td>

                    <td class="text-center">
                        <a href="#" title="Editar Instructor"><i class="glyphicon  glyphicon-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/Instructores/auto/<?php echo $filaTemp->id_aut; ?>" title="Eliminar Auto" onclick="return confirm('¿Seguro dese eliminar este registro?');" style="color:red;">
                            <i class="glyphicon  glyphicon-trash"></i>
                        </a>

                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <h1>No posee Autos</h1>
<?php endif; ?>