<h1>NUEVO SEMINARIO</h1>
<form action="<?php echo site_url('seminarios/proceActuali'); ?>" id="frm_editar_seminario" method="post" enctype="multipart/form-data">

    <div class="container">
        <div class="row">

            <input type="text" class="form-control" name="id_semi" id="id_semi" hidden value="<?php echo $seminarioEditar->id_semi; ?>">

            <div class="col-md-4">
                <label for="">Nombre:
                    <span class="obligatorio">(Campo Requerido)</span>
                </label>
                <br>
                <input type="text" placeholder="Ingrese el nombre del Seminario" class="form-control" required name="nom_semi" value="<?php echo $seminarioEditar->nom_semi; ?>">
            </div>
            <div class="col-md-4">
                <label for="">Duracion:
                    <span class="obligatorio">(Campo Requerido)</span>
                </label>
                <br>
                <input type="text" placeholder="Ingresar el tiempo de duracion" class="form-control" required name="dur_semi" value="<?php echo $seminarioEditar->dur_semi; ?>">
            </div>
            <div class="col-md-4">
                <label for="">Costo:
                    <span class="obligatorio">(Campo Requerido)</span>
                </label>
                <br>
                <input type="text" placeholder="Ingresar el costo para los estudiantes" class="form-control" required name="cos_semi" value="<?php echo $seminarioEditar->cos_semi; ?>">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-2"></div>

            <div class="col-md-8">
                <label for="">Contenido: </label>
                <input type="file" name="cont_semi" id="cont_semi" >
            </div>
            <div class="col-md-2"></div>
        </div>

        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    Editar
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/seminarios/listSem" class="btn btn-danger">Cancelar</a>
            </div>
        </div>
    </div>

</form>

<script type="text/javascript">
    $("#frm_editar_seminario").validate({
        rules: {

            nom_semi: {
                required: true,
                minlength: 3,
                maxlength: 400,
                letras: true,
            },

            dur_semi: {
                required: true,
                minlength: 5,
                maxlength: 250,

            },
            cos_semi: {
                required: true,
                minlength: 3,
                maxlength: 20,

            },

        },
        messages: {
            nom_semi: {
                required: "Por favor ingrese el nombre del seminario",
                minlength: "El seminario debe tener al menos 3 caracteres",
                maxlength: "Nombre de Seminario incorrecto",
            },
            dur_semi: {
                required: "Por favor ingrese la duracion del seminario",
                minlength: "Colocar con las palabras horas o minutos",
                maxlength: "duracion incorrecta",
            },
            cos_semi: {
                required: "Por favor ingresar el costo del seminario",
                minlength: "colocar un precio aceptable",
                maxlength: "precio execivo",
            },

        }
    });
</script>





<script type="text/javascript">
    $("#cont_semi").fileinput({
        language: 'es'
    });
</script>