<div class="row">
    <div class="col-md-8">
        <h1 Align="center">LISTADO DE Seminarios</h1>
    </div>
    <div class="col-md-4">
        <a href="<?php echo site_url('seminarios/nuevoSem') ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"> Agregar Seminario</i></a>
    </div>
</div>
<br>
<?php if ($seminarios) : ?>
    <table class="table table-striped table-border table-hover" id="tbl_seminarios">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE SEMINARIO</th>
                <th>DURACION DEL SEMINARIO</th>
                <th>COSTO DEL SEMINARIO</th>
                <th>CONTENIDO DEL SEMINARIO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($seminarios as $filaTemporal) : ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_semi; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nom_semi; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->dur_semi; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->cos_semi; ?>
                    </td>
                    <td>
                        <?php if ($filaTemporal->cont_semi != "") : ?>
                            <a href="<?php echo base_url('uploads/') . $filaTemporal->cont_semi; ?>" target="_blank">
                            <?php echo $filaTemporal->cont_semi; ?>
                            </a>
                        <?php else : ?>
                            N/A
                        <?php endif; ?>
                    </td>

                    <td class="text-center">
                        <a href="<?php echo site_url(); ?>/Seminarios/editSem/<?php echo $filaTemporal->id_semi; ?>" title="Editar Seminario"><i class="mdi mdi-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <?php if ($this->session->userdata("conectado")->perfil_usu == "Administrador") : ?>
                            <a href="<?php echo site_url(); ?>/Seminarios/eliminar/<?php echo $filaTemporal->id_semi; ?>" title="Eliminar Seminario" onclick="return confirm('¿Seguro dese eliminar este registro?');" style="color:red;">
                                <i class="mdi mdi-delete"></i>
                                Eliminar
                            </a>

                        <?php endif; ?>

                    </td>
                </tr>

            <?php endforeach; ?>

        </tbody>
    </table>
<?php else : ?>
    <h1>No posee Seminarios</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_seminarios").DataTable();
</script>