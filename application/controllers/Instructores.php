<?php
class Instructores extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    //cargar modelo    
    $this->load->model('Instructor');
    if (!$this->session->userdata("conectado")) {
      redirect("welcome/login");
    }
  }



  public function index()
  {
    $data['instructores'] = $this->Instructor->obtenerTodos();
    // print_r($data);
    $this->load->view('header');
    $this->load->view('instructores/index', $data);
    $this->load->view('footer');
  }

  public function nuevo()
  {
    $this->load->view('header');
    $this->load->view('instructores/nuevo');
    $this->load->view('footer');
  }

  public function auto()
  {
    $this->load->view('header');
    $this->load->view('instructores/auto');
    $this->load->view('footer');
  }

  public function listadoauto()
  {
    $autos['instructores'] = $this->Instructor->obtenerAutos();
    $this->load->view('header');
    $this->load->view('instructores/listadoauto', $autos);
    $this->load->view('footer');
  }




  public function guardar()
  {
    $datosNuevoInstructor = array(
      "cedula_ins" => $this->input->post('cedula_ins'),
      "primer_apellido_ins" => $this->input->post('primer_apellido_ins'),
      "segundo_apellido_ins" => $this->input->post('segundo_apellido_ins'),
      "nombres_ins" => $this->input->post('nombres_ins'),
      "titulo_ins" => $this->input->post('titulo_ins'),
      "telefono_ins" => $this->input->post('telefono_ins'),
      "direccion_ins" => $this->input->post('direccion_ins')
    );
    $this->load->library("upload");
    $new_name = "foto_instructor_" . time() . "_" . rand(1, 5000);
    $config['file_name'] = $new_name . '_1';
    $config['upload_path']          = FCPATH . 'uploads/';
    $config['allowed_types']        = 'jpeg|jpg|png';
    $config['max_size']             = 1024 * 5;
    $this->upload->initialize($config);

    if ($this->upload->do_upload("foto_ins")) {
      $dataSubida = $this->upload->data();
      $datosNuevoInstructor["foto_ins"] = $dataSubida['file_name'];
    }


    if ($this->Instructor->insertar($datosNuevoInstructor)) {
      $this->session->set_flashdata("confirmacion", "Instructor guardado con EXITO!!!");
    } else {
      $this->session->set_flashdata("ERROR", "NO se ha logrado guardar el instructor......intente nuevamente");
    }
    redirect('instructores/index');
  }

  public function eliminar($id_ins)
  {
    if ($this->session->userdata("conectado")->perfil_usu != "Administrador") {
      $this->session->set_flashdata("ERROR", "No tiene permisos para eliminar");
      redirect("instructores/index");
    }

    if ($this->Instructor->borrar($id_ins)) {
      $this->session->set_flashdata("confirmacion", "Instructor Eliminado con EXITO!!!");
    } else {
      $this->session->set_flashdata("ERROR", "NO se ha logrado Eliminar el instructor......intente nuevamente");
    }
    redirect('Instructores/index');
  }

  public function editar($id_ins)
  {
    $data["instructorEditar"] = $this->Instructor->ObtenerPorId($id_ins);
    $this->load->view('header');
    $this->load->view('instructores/editar', $data);
    $this->load->view('footer');
  }

  public function proceActuali()
  {
    $datosEditados = array(
      "cedula_ins" => $this->input->post('cedula_ins'),
      "primer_apellido_ins" => $this->input->post('primer_apellido_ins'),
      "segundo_apellido_ins" => $this->input->post('segundo_apellido_ins'),
      "nombres_ins" => $this->input->post('nombres_ins'),
      "titulo_ins" => $this->input->post('titulo_ins'),
      "telefono_ins" => $this->input->post('telefono_ins'),
      "direccion_ins" => $this->input->post('direccion_ins')
    );
    $id_ins = $this->input->post("id_ins");
    if ($this->Instructor->actualizar($id_ins, $datosEditados)) {
      $this->session->set_flashdata("confirmacion", "Instructor EDITADO con EXITO!!!");
    } else {
      $this->session->set_flashdata("ERROR", "NO se ha logrado EDITAR el instructor......intente nuevamente");
    }
    redirect("instructores/index");
  }

  public function guardaraut()
  {
    $datosAutoNuevo = array(
      "nom_aut" => $this->input->post('nom_aut'),
      "col_aut" => $this->input->post('col_aut'),
      "pre_aut" => $this->input->post('pre_aut'),
      "kilo_aut" => $this->input->post('kilo_aut'),
      "an_aut" => $this->input->post('an_aut'),
      "trans_aut" => $this->input->post('trans_aut'),
      "desc_aut" => $this->input->post('desc_aut')
    );
    if ($this->Instructor->insertarauto($datosAutoNuevo)) {
      redirect('instructores/listadoauto');
    } else {
      echo "<h1>NO SE HAN INGRESADO DATOS</h1>";
    }
  }

  public function eliminarAuto($id_aut)
  {
    if ($this->Instructor->borrarAuto($id_aut)) {
      redirect('Instructores/auto');
    } else {
      echo "ERROR AL BORRAR :( ";
    }
  }
}
