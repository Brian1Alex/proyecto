<?php
class Seminarios extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Seminario');
        if (!$this->session->userdata("conectado")) {
            redirect("welcome/login");
        }
    }

    public function nuevoSem()
    {
        $this->load->view('header');
        $this->load->view('seminarios/nuevoSem');
        $this->load->view('footer');
    }

    public function listSem()
    {
        $seminarios['seminarios'] = $this->Seminario->obtenerSemi();
        $this->load->view('header');
        $this->load->view('seminarios/listSem', $seminarios);
        $this->load->view('footer');
    }

    public function editSem($id_semi)
    {
        $data["seminarioEditar"] = $this->Seminario->ObtenerPorId($id_semi);
        $this->load->view('header');
        $this->load->view('seminarios/editSem', $data);
        $this->load->view('footer');
    }


    public function guardar()
    {
        $datosNueSemi = array(
            "nom_semi" => $this->input->post('nom_semi'),
            "dur_semi" => $this->input->post('dur_semi'),
            "cos_semi" => $this->input->post('cos_semi')
        );
        $this->load->library("upload");
        $new_name = "contenido_seminario_" . time() . "_" . rand(1, 5000);
        $config['file_name'] = $new_name . '_1';
        $config['upload_path']          = FCPATH . 'uploads/';
        $config['allowed_types']        = 'pdf';
        $config['max_size']             = 1024 * 5;
        $this->upload->initialize($config);

        if ($this->upload->do_upload("cont_semi")) {
            $dataSubida = $this->upload->data();
            $datosNueSemi["cont_semi"] = $dataSubida['file_name'];
        }

        if ($this->Seminario->insertar($datosNueSemi)) {
            $this->session->set_flashdata("confirmacion", "Seminario guardado con EXITO!!!");
        } else {
            $this->session->set_flashdata("ERROR", "NO se ha logrado guardar el Seminario......intente nuevamente");
        }
        redirect('seminarios/listSem');
    }

    public function eliminar($id_semi)
    {
        if ($this->session->userdata("conectado")->perfil_usu != "Administrador") {
            $this->session->set_flashdata("ERROR", "No tiene permisos para eliminar");
            redirect("seminarios/listSem");
        }

        if ($this->Seminario->borrar($id_semi)) {
            $this->session->set_flashdata("confirmacion", "Seminario Eliminado con EXITO!!!");
        } else {
            $this->session->set_flashdata("ERROR", "NO se ha logrado Eliminar el Seminario......intente nuevamente");
        }
        redirect('seminarios/listSem');
    }

    public function proceActuali()
    {
        $datosEditados = array(
            "nom_semi" => $this->input->post('nom_semi'),
            "dur_semi" => $this->input->post('dur_semi'),
            "cos_semi" => $this->input->post('cos_semi')
        );
        $this->load->library("upload");
        $new_name = "contenido_seminario_" . time() . "_" . rand(1, 5000);
        $config['file_name'] = $new_name . '_1';
        $config['upload_path']          = FCPATH . 'uploads/';
        $config['allowed_types']        = 'pdf';
        $config['max_size']             = 1024 * 5;
        $this->upload->initialize($config);

        if ($this->upload->do_upload("cont_semi")) {
            $dataSubida = $this->upload->data();
            $datosEditados["cont_semi"] = $dataSubida['file_name'];
        }

        $id_semi = $this->input->post("id_semi");
        if ($this->Seminario->actualizar($id_semi, $datosEditados)) {
            $this->session->set_flashdata("confirmacion", "Seminario EDITADO con EXITO!!!");
        } else {
            $this->session->set_flashdata("ERROR", "NO se ha logrado EDITAR el Seminario......intente nuevamente");
        }
        redirect("seminarios/listSem");
    }
}
