<?php
class Talleres extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        //Cargar modelo del model
        $this->load->model('Taller');
        if (!$this->session->userdata("conectado")) {
            redirect("welcome/login");
        }
        
    }
    //Funcion que renderiza la vista index, listar
    public function index()
    {
        //data es una convencion pero puede llamarse de otra forma
        // print_r($instructores);
        // $listadoInstructores
        $this->load->view('header');
        $this->load->view('talleres/index');
        $this->load->view('footer');
    }
    //vista de nuevo
    public function listado()
    {
        $data['talleres'] = $this->Taller->obtenerTodos();
        $this->load->view('talleres/listar', $data);
    }
    //vista de editar
    public function editar($id_semi)
    {

        $datos['editarTaller'] = $this->Taller->obtenerPorId($id_semi);
        $this->load->view('Tallers/editar', $datos);
    }
    //Fucion para guardar
    public function guardar()
    {
        $datosEditados = array(
            "nom_semi" => $this->input->post('nom_semi'),
            "dur_semi" => $this->input->post('dur_semi'),
            "cos_semi" => $this->input->post('cos_semi'),
        );
        //Codigo para subir Foto
        $this->load->library("upload");
        $new_name = "archivo_Taller_" . time() . "_" . rand(1, 5000);
        $config['file_name'] = $new_name . '_1';
        $config['upload_path']          = FCPATH . 'uploads/contenido';
        $config['allowed_types']        = 'pdf';
        $config['max_size']             = 1024*5;//confiugtacion del tamaño del archivo MB
        $this->upload->initialize($config);
        if ($this->upload->do_upload("contenido_wslc")) {
            $dataSubida = $this->upload->data();
            $datosEditados["contenido_wslc"] = $dataSubida['file_name'];
        }
        //print_r($datosNuevoInstructor);
        //$this->Instructor->insertar($datosNuevoInstructor);
        if ($this->Taller->insertar($datosEditados)) {
            $resultado = array("estado"=>"ok");
            //$this->session->set_flashdata("confirmacion", "Taller guardado exitosamente");
        } else {
            //enbeber codigo
            echo "<h1>ERROR AL INSERTAR</h1>";
            $resultado = array("estado"=>"error");
            //$this->session->set_flashdata("error", "Taller no guardado");
        }
        echo json_encode($resultado);
    }
    //funcion eliminar
    public function eliminar($id_semi)
    {
        if($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
            $this->session->set_flashdata('error', 'No tiene permisos para eliminar');
            redirect('Tallers/index');
        }
        if ($this->Taller->borrar($id_semi)) {
            redirect('Tallers/index');
            $resultado = array("estado"=>"ok");
        } else {
            $resultado = array("estado"=>"error");
            //echo "ERROR AL BORRAR :(";
        }
        //echo json_encode($resultado);

    }
    //funcion para el modelo de editar
    //ACTUALIZAR
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "nom_semi" => $this->input->post('nom_semi'),
            "dur_semi" => $this->input->post('dur_semi'),
            "cos_semi" => $this->input->post('cos_semi'),
        );
        //Codigo para subir Foto
        //Comprobar si existe el archivo

        //print_r($datosNuevoInstructor);
        //$this->Instructor->insertar($datosNuevoInstructor);
        $id_semi = $this->input->post("id_semi");
        if ($this->Taller->actualizar($id_semi,$datosEditados)) {
            $resultado = array("estado"=>"ok");
            redirect("Tallers/index");
            //$this->session->set_flashdata("confirmacion", "Taller guardado exitosamente");
        } else {
            //enbeber codigo
            echo "<h1>ERROR AL INSERTAR</h1>";
            $resultado = array("estado"=>"error");
            //$this->session->set_flashdata("error", "Taller no guardado");
        }
        echo json_encode($resultado);

    }
}
