<?php
class Taller extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar datos
    function insertar($datos)
    {
        //Active Record en code igniter SQL Injection
        //return devuelve un valor true o false.
        return $this->db->insert("seminario", $datos);
    }
    //Funciones para consultar Instructores
    function obtenerTodos()
    {
        //Condicional para validar si hay datos
        $listadoSeminarios = $this->db->get("seminario");
        if ($listadoSeminarios->num_rows() > 0) { //si hay datos
            return $listadoSeminarios->result();
        } else {
            return false;
        }
    }
    //borrar instructores
    public function borrar($id_semi)
    {
        /////////////////Borrar archivo/////////////
        $this->db->select('cont_semi');
        $this->db->where('id_semi', $id_semi); // Condición WHERE para seleccionar por su ID
        $query = $this->db->get('seminario');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $nombre = $row->cont_semi;
            //echo "El nombre del archivo es: " . $nombre;
            unlink("uploads/contenido/".$nombre);
        }
        $this->db->where('id_semi', $id_semi);
        return $this->db->delete("seminario");
    }

    
    //FUNCION PARA CONSULTRA UN INSTRUCTOR ESPECIFICO PARA EDITAR
    function obtenerPorId($id_semi)
    {
        $this->db->where("id_semi", $id_semi);
        $instructor = $this->db->get("seminario");
        if ($instructor->num_rows() > 0) {
            return $instructor->row();
        }
        return false;
    }
    //FUNCION PARA ACTUALIZAR Instructor
    function actualizar($id_semi, $datos)
    {
        $this->db->where("id_semi", $id_semi);
        return $this->db->update('seminario', $datos);
    }
}
