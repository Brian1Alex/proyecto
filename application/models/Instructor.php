<?php
class Instructor extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("instructor", $datos);
    }
    function obtenerTodos()
    {
        $listadoInstructores = $this->db->get("instructor");
        if ($listadoInstructores->num_rows() > 0) {
            return $listadoInstructores->result();
        } else {
            return false;
        }
    }

    function borrar($id_ins)
    {
        $this->db->where("id_ins", $id_ins);
        return $this->db->delete("instructor");
    }

    function ObtenerPorId($id_ins)
    {
        $this->db->where("id_ins", $id_ins);
        $instructor = $this->db->get("instructor");
        if ($instructor->num_rows() > 0) {
            return $instructor->row();
        }
        return false;
    }

    function actualizar($id_ins, $datos)
    {
        $this->db->where("id_ins", $id_ins);
        return $this->db->update('instructor', $datos);
    }

    function insertarauto($automovil)
    {
        return $this->db->insert("automovil", $automovil);
    }

    function obtenerAutos()
    {
        $listadoAutos = $this->db->get("automovil");
        if ($listadoAutos->num_rows() > 0) {
            return $listadoAutos->result();
        } else {
            return false;
        }
    }

    function borrarAuto($id_aut)
    {
        $this->db->where("id_aut", $id_aut);
        return $this->db->delete("automovil");
    }
}
