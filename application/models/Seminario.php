<?php
class Seminario extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("seminario", $datos);
    }

    function obtenerSemi()
    {
        $listaSemina = $this->db->get("seminario");
        if ($listaSemina->num_rows() > 0) {
            return $listaSemina->result();
        } else {
            return false;
        }
    }

    function borrar($id_semi)
    {
        $this->db->where("id_semi", $id_semi);
        return $this->db->delete("seminario");
    }

    function ObtenerPorId($id_semi)
    {
        $this->db->where("id_semi", $id_semi);
        $seminario = $this->db->get("seminario");
        if ($seminario->num_rows() > 0) {
            return $seminario->row();
        }
        return false;
    }

    function actualizar($id_semi, $datos)
    {
        $this->db->where("id_semi", $id_semi);
        return $this->db->update('seminario', $datos);
    }
}
